import express from 'express';
import dotenv from 'dotenv';
import * as db from '../config/database';
import config from 'config';
dotenv.config();
db.connect();

const app = express();
const port = config.get<number>('server.port');

app.use(express.json());
app.listen(port, () => {
  console.log(`Server started on port: ${port}`);
  console.log();
});
