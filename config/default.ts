export default {
    server: {
        port: 3000,
    },
    accessTokenExpiresIn: 15,
};